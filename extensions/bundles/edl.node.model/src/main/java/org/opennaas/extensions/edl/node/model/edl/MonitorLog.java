package org.opennaas.extensions.edl.node.model.edl;

/*
 * #%L
 * OpenNaaS :: EDL :: NODE
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.opennaas.extensions.edl.node.model.edl.Load;

import java.util.Set;

//rdf("http://www.science.uva.nl/research/sne/edl.owl#MonitorLog")
public class MonitorLog {
	private Set<Load> loads;
	private int duration;
	private int interval;
	
	//rdf("http://www.science.uva.nl/research/sne/edl.owl#includeLoad")
	public Set<Load> getEdlIncludeLoad(){
		return loads;
	}
	public void setEdlIncludeLoad(Set<Load> edlIncludeLoad){
		this.loads = edlIncludeLoad;
	}

	//rdf("http://www.science.uva.nl/research/sne/edl.owl#sampleDuration")
	public Integer getEdlSampleDuration(){
		return duration;
	}
	public void setEdlSampleDuration(Integer edlSampleDuration){
		this.duration = edlSampleDuration;
	}

	//rdf("http://www.science.uva.nl/research/sne/edl.owl#sampleInterval")
	public Integer getEdlSampleInterval(){
		return interval;
	}
	public void setEdlSampleInterval(Integer edlSampleInterval){
		this.interval = edlSampleInterval;
	}
	
}
