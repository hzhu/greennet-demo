package org.opennaas.extensions.edl.node.capability.shell;

/*
 * #%L
 * OpenNaaS :: EDL :: NODE
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.felix.gogo.commands.Argument;
import org.apache.felix.gogo.commands.Command;
import org.opennaas.core.resources.IResource;
import org.opennaas.core.resources.shell.GenericKarafCommand;
import org.opennaas.extensions.edl.node.capability.IPowerSetupCapability;
import org.opennaas.extensions.edl.node.model.edl.EDLNode;
import org.opennaas.extensions.edl.node.model.edl.Load;


@Command(scope = "edl", name = "deleteMetric", 
description = "Delete a metric")
public class DeleteMetricCommand extends GenericKarafCommand{

	@Argument(index = 0, name = "resourceType:resourceName", description = "The resource id", required = true, multiValued = false)
	private String	resourceName;

	@Argument(index = 1, name = "metricId", description = "The id of the metric.",
			required = true, multiValued = false)
	private String	metricId;

	@Override
	protected Object doExecute() throws Exception {
		printInitCommand("deleteMetric");
		try {
			IResource resource = getResourceFromFriendlyName(resourceName);
			IPowerSetupCapability capab = (IPowerSetupCapability) resource
					.getCapabilityByInterface(IPowerSetupCapability.class);
			capab.deleteMetric(metricId);
			printInfo("Deleted metric " + metricId);
			printInfo("Already selected metrics: ");
			EDLNode node = (EDLNode) resource.getModel();
			for(Load load: node.getEdlHasLog().getEdlIncludeLoad()){
				printSymbol(load.getEdlMaptoMetric().getEdlHasMetricName());
			}
		} catch (Exception e) {
			printError("Error in deleteMetric " + resourceName + " with metric id " + metricId);
			printError(e);
		} finally {
			printEndCommand();
		}
		printEndCommand();
		return null;
	}


}
