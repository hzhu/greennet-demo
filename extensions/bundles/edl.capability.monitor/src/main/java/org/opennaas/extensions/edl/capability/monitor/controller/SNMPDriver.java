package org.opennaas.extensions.edl.capability.monitor.controller;

/*
 * #%L
 * OpenNaaS :: EDL ::PowerMonitor
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import org.snmp4j.CommunityTarget; 
import org.snmp4j.PDU; 
import org.snmp4j.ScopedPDU;
import org.snmp4j.Snmp; 
import org.snmp4j.TransportMapping; 
import org.snmp4j.UserTarget;
import org.snmp4j.event.ResponseEvent; 
import org.snmp4j.event.ResponseListener; 
import org.snmp4j.mp.SnmpConstants; 
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.smi.*; 
import org.snmp4j.transport.DefaultUdpTransportMapping;


public class SNMPDriver {
	public static final int mSNMPVersion =0; // 0 represents SNMP version=1 
	String address = null;
	Snmp snmp = null;
	int version; 
	
	public SNMPDriver(String urlAddress){
		address = urlAddress ;
	}
	
	public String getSNMPToString(String community, String oid) throws IOException{
		String str="";
		Set<String> oids = new LinkedHashSet<String>();
		oids.add(oid);
		PDU pduresponse = getSNMP(community, oids);
		str = pduresponse.get(0).getVariable().toString();
		//System.out.println("Response="+str);
		return str;	
	}
	
	public Set<String> getSNMPToStringSet(String community, Set<String> oids) throws IOException{
		String str = "";
		Set<String> strs = new LinkedHashSet<String>();
		PDU pduresponse = getSNMP(community, oids);
		int it =0;
		while( it < pduresponse.getVariableBindings().size()){
			str = pduresponse.get(it).getVariable().toString();
			strs.add(str);
			it ++;
		}			
		//System.out.println("Response="+str);
		return strs;	
	}
	
	
	public long getSNMPToInteger(String community, String oid) throws IOException{
		Set<String> oids = new LinkedHashSet<String>();
		oids.add(oid);
		PDU pduresponse = getSNMP(community, oids);
		return pduresponse.get(0).getVariable().toLong();
		
	}
	
	public void setupListener() throws IOException{
		TransportMapping transport = new DefaultUdpTransportMapping(); 
		snmp = new Snmp(transport); 
		try{
			transport.listen(); 
		}catch(IOException e){
			System.out.println("Error set up SNMP listener");
			e.printStackTrace(); 
		}
	}
	
	public void setSNMP(String community, String oid, int value) throws IOException{
		try{
			Address targetAddress = GenericAddress.parse(address);
			CommunityTarget target = new CommunityTarget(); 
			target.setCommunity(new OctetString(community)); 
			target.setAddress(targetAddress); 
			target.setRetries(2); 
			target.setTimeout(5000); 
			target.setVersion(SnmpConstants.version1);
			
			PDU pdu = new PDU();
			pdu.add(new VariableBinding(new OID(oid), new Integer32(value))); 
			pdu.setType(PDU.SET); 
			ResponseListener listener = new ResponseListener() {
				public void onResponse(ResponseEvent event) { 
					 // Always cancel async request when response has been received 
					 // otherwise a memory leak is created! Not canceling a request 
					 // immediately can be useful when sending a request to a broadcast 
					 // address. 
					 ((Snmp)event.getSource()).cancel(event.getRequest(), this); 
					 System.out.println("Set Status is: " + event.getResponse().getErrorStatusText()); 
				}
			};
			snmp.send(pdu, target, null, listener);


		}catch (IOException e){
			System.out.println("Error SNMP SET.");
			e.printStackTrace(); 
		}
	}
	
	public PDU getSNMP(String community, Set<String> oids) throws IOException{
			Address targetaddress = new UdpAddress(address);
			
			CommunityTarget comtarget = new CommunityTarget(); 
			comtarget.setCommunity(new OctetString(community)); 
			comtarget.setVersion(SnmpConstants.version1); 
			comtarget.setAddress(targetaddress); 
			comtarget.setRetries(1); 
			comtarget.setTimeout(5000);
					 
			PDU pdu = new PDU(); 
			ResponseEvent response = null;
			for (String oid:oids){
				pdu.add(new VariableBinding(new OID(oid))); 
			}
			pdu.setType(PDU.GET);
			response = snmp.get(pdu, comtarget);
			if(response.getResponse() != null){
				if(response.getResponse().getErrorStatusText().equalsIgnoreCase("Success")){
					PDU pduresponse=response.getResponse();
					//System.out.println(pduresponse);
					return pduresponse;
				}
				String msg= response.getResponse().getErrorStatusText().toString();
				System.out.println(msg);
				throw new IOException("SNMP Error: " + msg);

			}else{
				throw new IOException("A SNMP GET timeout, check the network connection.");
			}
	}
	
	

}
