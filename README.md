
OpenNaaS with GreenNet, which is based on OpenNaaS with NFV(0.24.2)

Three new energy-aware capabilities in OpenNaaS with GreenNet:
1. Energy description capability(EDL): create and manage info about Energy source, Power meters, Green metric, Power state etc.
2. Energy monitoring capability: obtain data in
 1) Observed metrics: Power, energy, CO2 emission rate, electricity price
 2) Calculated metrics: Total Electricity, CO2 emission,  energy efficiency
3. Green routing capability in terms of power, cost and emission metrics.

The target of Green Routing is to look for a routing path, which consists of network devices with the least energy consumption, cost or CO2 emission.

Three functions of green routing gui demo:
1. Show power in info of each node.
2. Show power info of each specified routing path.
3. Find green routing path using three green metrics.
  Power consumption is stable for today’s network devices
  Calculate a static routing path for specified starting and destination nodes





OpenNaaS with NFV - Routing bundle
========

 ** Supports Floodlight controller and OpenDaylight controller. Also manages the VTN Coordinator in order to manage the OpenDaylight resources. **
 ** The VTN coordinator makes the portmapping according with the route found by the VRF module of OpenNaaS. **

In order for NRENs and operators to be able to deploy and operate innovative NaaS offerings, an appropriate toolset needs to be created. With such goal in mind, Mantychore FP7 has created the OpenNaaS framework.

OpenNaaS was born with the aim to create an open source software project community that allows several stakeholders to contribute and benefit from a common NaaS software stack. OpenNaaS offers a versatile toolset for the deployment of NaaS oriented services. The software is released with a dual L-GPL/ASF licensing schema that ensures that the platform will remain open and consistent, while commercial derivatives can be built on top. This open schema allows trust to be built on the platform, as NRENs and commercial network operators can rely on the continuity, transparency and adaptability of the platform.

In that sense, each network domain would be able to use their own OpenNaaS instance to:

 * Get resources from the network infrastructure: routers, switches, links or provisioning systems.
 * Abstract them to service resources, independently of vendor and model details.
 * Embed instantiation of virtualized resources in the regular BSS workflows.
 * Delegate management permissions over the infrastructure resources they own so that “Infrastructure integrators” can control them during a period of time.

With an eye on versatility and smooth integration, OpenNaaS offers a powerful remote command line, as well as web-service interfaces. This web-service interface will offer the possibility to both build a GUI and integrate it with existing middleware applications already deployed in the virtual research organisations.

Get the software
----------------

For instructions on building the software go to:

http://www.opennaas.org/download/

Developer resources here:

http://www.opennaas.org/development/

You can find documentation pointers here:

http://www.opennaas.org/documentation/

Authors
-------

Join us here:

http://www.opennaas.org/community/

* Adrian Rossello
* Carlos Baez
* Eduard Grasa
* Elisabeth Rigol
* Gerd Behrmann
* Isart Canyameres
* Josep Batallé
* Jordi Puig
* Natalia Koroleva
* Pau Minoves
* Santiago Pimentel
