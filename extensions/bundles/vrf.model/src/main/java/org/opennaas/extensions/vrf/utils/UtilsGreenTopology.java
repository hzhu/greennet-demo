package org.opennaas.extensions.vrf.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.MappingJsonFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.opennaas.core.resources.ActivatorException;
import org.opennaas.core.resources.IResource;
import org.opennaas.core.resources.IResourceIdentifier;
import org.opennaas.core.resources.IResourceManager;
import org.opennaas.core.resources.ResourceException;
import org.opennaas.extensions.edl.capability.monitor.capability.IPowerMonitorCapability;
import org.opennaas.extensions.edl.node.model.edl.EnergySource;
import org.opennaas.extensions.edl.node.model.edl.Load;
import org.opennaas.extensions.edl.node.model.edl.Measurement;
import org.opennaas.extensions.edl.node.model.edl.MonitorLog;
import org.opennaas.extensions.vrf.model.topology.Edge;
import org.opennaas.extensions.vrf.model.topology.TopologyInfo;
import org.opennaas.extensions.vrf.model.topology.Vertex;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class UtilsGreenTopology {
	
    static Log log = LogFactory.getLog(UtilsGreenTopology.class);
    
    
	public static TopologyInfo createGreenAdjacencyMatrix(String topologyFilename, String metric) {

        List<Vertex> nodes = new ArrayList<Vertex>();
        List<Edge> edges = new ArrayList<Edge>();


        try {
            JsonFactory f = new MappingJsonFactory();
            JsonParser jp = f.createJsonParser(new File(topologyFilename));
            JsonToken current = jp.nextToken();
            if (current != JsonToken.START_OBJECT) {
                Logger.getLogger(UtilsTopology.class.getName()).log(Level.SEVERE, null, "Error: root should be object: quiting.");
            }
            while (jp.nextToken() != JsonToken.END_OBJECT) {
                String fieldName = jp.getCurrentName();
                current = jp.nextToken();// move from field name to field value
                if (fieldName.equals("nodes")) {
                    if (current == JsonToken.START_ARRAY) {
                        while (jp.nextToken() != JsonToken.END_ARRAY) {
                        	float powerCost = 1;
                            JsonNode node = jp.readValueAsTree();
                            int type;
                            if (node.get("type").getValueAsText().equals("sw")) {
                                type = 0;
                                Float costFl = getSwPowerInfo(node.get("name").getValueAsText());
                                
                                if (metric == "power"){
                                    log.error("Power info of resource" + node.get("name").getValueAsText() + ": " + costFl);

                                }else if (metric == "cost"){
                                	EnergySource es = getSwEnergySourceInfo(node.get("name").getValueAsText());
                                    Float price = es.getEdlElectricityPrice();
                                    log.error("Power/price info of resource" + node.get("name").getValueAsText() + ": " + costFl +"/"+ price);
                                    costFl = costFl*price; 
                                    
                                }else if (metric == "emission"){
                                	EnergySource es = getSwEnergySourceInfo(node.get("name").getValueAsText());
                                    Float emission = es.getEdlEmissionPerUnitofEnergy();
                                    log.error("Power/emission info of resource" + node.get("name").getValueAsText() + ": " + costFl +"/"+ emission);
                                    costFl = costFl*emission; 
                                    
                                }
                                
                                powerCost = costFl;
                                
                            } else {
                                type = 1;
                            }
                
                            Vertex v = new Vertex(powerCost, node.get("id").getValueAsText(),
                                    node.get("dpid").getValueAsText(),
                                    type);
                            nodes.add(v);
                        }
                    } else {
                        jp.skipChildren();
                    }
                } else if (fieldName.equals("links")) {
                    if (current == JsonToken.START_ARRAY) {
                        // For each of the records in the array
                        while (jp.nextToken() != JsonToken.END_ARRAY) {
                            JsonNode link = jp.readValueAsTree();
                            String srcId = link.get("source").getValueAsText();
                            String dstId = link.get("target").getValueAsText();
                            int srcPort = Integer.parseInt(link.get("srcP").getValueAsText());
                            int dstPort = Integer.parseInt(link.get("dstP").getValueAsText());
                            Vertex srcV = null;
                            Vertex dstV = null;
                            float dijkstraCost;
                            
                            for (Vertex v : nodes) {
                                if (v.getId().equals(srcId)) {
                                    //srcV = new Vertex(v.getId(), v.getDPID(), v.getType()); BY HAO ZHU
                                	srcV = v;
                                } else if (v.getId().equals(dstId)) {
                                    //dstV = new Vertex(v.getId(), v.getDPID(), v.getType()); BY HAO ZHU
                                	dstV = v;
                                }
                            }
                            dijkstraCost = dstV.getPower();
                            log.error("dijkstraCost: " + dijkstraCost);
                            Edge e = new Edge(link.get("id").getValueAsText(), srcV, dstV, dijkstraCost, srcPort, dstPort);
                            edges.add(e);
                            dijkstraCost = srcV.getPower();
                            e = new Edge(link.get("id").getValueAsText() + "-", dstV, srcV, dijkstraCost, dstPort, srcPort);
                            edges.add(e);
                        }
                    } else {
                        jp.skipChildren();
                    }
                } else {
                    jp.skipChildren();
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(UtilsTopology.class.getName()).log(Level.SEVERE, null, ex);
        }
        TopologyInfo topoInfo = new TopologyInfo(nodes, edges);
        return topoInfo;
    }
    
    /**
     * Call a rest service to get an monitor log in observed green metrics
     *
     * @param switch name
     * @return power consumption 
     */
    private static Float getSwPowerInfo(String name) {
        Float power = null;
        try {
        	IResource resource = getResourceByName(name);
        	log.error("Calling sw power INFO");
        	MonitorLog mlog = ((IPowerMonitorCapability) resource.getCapabilityByInterface(IPowerMonitorCapability.class))
					.getCurrentObservedLog();
            //log.info("Monitor log info: " + mlog);
            
            for(Load load: mlog.getEdlIncludeLoad()){
            	if(load.getEdlMaptoMetric().getEdlHasMetricName().equals("PowerConsumption")){
            		Measurement mm =load.getEdlHasMeasurement().iterator().next();
            		power = mm.getEdlMetricvalue();
            	}	
            	
            }
            
        } catch (ActivatorException ex) {
            Logger.getLogger(UtilsTopology.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ResourceException ex) {
            Logger.getLogger(UtilsTopology.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
        	Logger.getLogger(UtilsTopology.class.getName()).log(Level.SEVERE, null, ex);
		}

  
        return power;
    }
    
    /**
     * Call a rest service to get current energy source being used
     *
     * @param switch name
     * @return power consumption 
     */
    private static EnergySource getSwEnergySourceInfo(String name) {
        ClientResponse response;
        //String resourceName = getEDLNodeResourceName(name);//request the EDLNode resourceName
        String resourceName = name;
        String emission = null;
        String price = null;
        EnergySource esInfo = null;
            
        try {
        	IResource resource = getResourceByName(name);
        	log.error("Calling sw energy source INFO");
        	EnergySource es = ((IPowerMonitorCapability) resource.getCapabilityByInterface(IPowerMonitorCapability.class))
					.getEnergySource();
        	esInfo = es;
        	//log.info("Energy source info: " + esInfo);
                    
        } catch (ActivatorException ex) {
            Logger.getLogger(UtilsTopology.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ResourceException ex) {
            Logger.getLogger(UtilsTopology.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
        	Logger.getLogger(UtilsTopology.class.getName()).log(Level.SEVERE, null, ex);
		}

        return esInfo;
    }
    
    private static IResource getResourceByName(String name) throws ActivatorException, ResourceException {
       // IResourceManager resourceManager = org.opennaas.extensions.sdnnetwork.Activator.getResourceManagerService();
        //IResource sdnNetResource = resourceManager.listResourcesByType("sdnnetwork").get(0);
        //IOFProvisioningNetworkCapability sdnCapab = (IOFProvisioningNetworkCapability) sdnNetResource.getCapabilityByInterface(IOFProvisioningNetworkCapability.class);
        String resourceName;
        IResourceManager resourceManager = org.opennaas.extensions.openflowswitch.repository.Activator.getResourceManagerService();
        List<IResource> listResources = resourceManager.listResourcesByType("openflowswitch");
        if (listResources == null) {
            log.error("This Openflow Switch resource is empty.");
            return null;
        }
        for (IResource r : listResources) {
        	resourceName = r.getResourceDescriptor().getInformation().getName();
            if (resourceName.equals(name)) {
                log.debug("Switch name is: " + resourceName);
            }
        }

        resourceName = name;
        IResourceIdentifier resourceId = resourceManager.getIdentifierFromResourceName("openflowswitch", resourceName);

        if (resourceId == null) {
            log.error("IResource id is null.");
            return null;
        }
        return resourceManager.getResource(resourceId);
    }

}
