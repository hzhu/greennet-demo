package org.opennaas.extensions.edl.node.capability.shell;

/*
 * #%L
 * OpenNaaS :: EDL :: NODE
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.felix.gogo.commands.Argument;
import org.apache.felix.gogo.commands.Command;
import org.opennaas.core.resources.IResource;
import org.opennaas.core.resources.shell.GenericKarafCommand;
import org.opennaas.extensions.edl.node.capability.IPowerSetupCapability;

@Command(scope = "edl", name = "setNodeOutlet", 
description = "Set the outlet of the resource")
public class SetNodeOutletCommand extends GenericKarafCommand{

	@Argument(index = 0, name = "resourceType:resourceName", 
			description = "The resource id", required = true, multiValued = false)
	private String	resourceName;

//	@Argument(index = 1, name = "pduId", description = "The id of the pdu.", 
//			required = true, multiValued = false)
//	private String	pduId;

	@Argument(index = 1, name = "moduleId", description = "The id of a pdu module",
			required = true, multiValued = false)
	private int	moduleNum;
	
	@Argument(index = 2, name = "portId", description = "The id of a pdu outlet",
			required = true, multiValued = false)
	private int	portNum;

	@Override
	protected Object doExecute() throws Exception {
		printInitCommand("setNodeOutlet");

		try {
			IResource resource = getResourceFromFriendlyName(resourceName);
			IPowerSetupCapability capab = (IPowerSetupCapability) resource
					.getCapabilityByInterface(IPowerSetupCapability.class);

			capab.setNodeOutlet(moduleNum, portNum);


		} catch (Exception e) {
			printError("Error in setNodeOutlet in resource " + 
					resourceName );
			printError(e);
		} finally {
			printEndCommand();
		}
		printEndCommand();
		return null;
	}

}
