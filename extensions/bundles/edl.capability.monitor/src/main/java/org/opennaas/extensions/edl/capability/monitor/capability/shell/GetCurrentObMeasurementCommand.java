package org.opennaas.extensions.edl.capability.monitor.capability.shell;

/*
 * #%L
 * OpenNaaS :: EDL ::PowerMonitor
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.felix.gogo.commands.Argument;
import org.apache.felix.gogo.commands.Command;
import org.opennaas.core.resources.IResource;
import org.opennaas.core.resources.shell.GenericKarafCommand;
import org.opennaas.extensions.edl.node.model.edl.Measurement;
import org.opennaas.extensions.edl.node.model.edl.MonitorLog;
import org.opennaas.extensions.edl.node.model.edl.Load;
import org.opennaas.extensions.edl.capability.monitor.capability.IPowerMonitorCapability;


@Command(scope = "edl", name = "getCurrentObMeasurement", 
	description = "Get current measurement of specified resource in observed green metrics")
public class GetCurrentObMeasurementCommand extends GenericKarafCommand {

	@Argument(index = 0, name = "resourceType:resourceName", 
			description = "The resource id to be monitored.", required = true, multiValued = false)
	private String		resourceId;

	private DateFormat	dateFormat	= new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	@Override
	protected Object doExecute() throws Exception {

		printInitCommand("getCurrentObservedPowerMeasurement of resource: " + resourceId);

		try{
			IResource resource = getResourceFromFriendlyName(resourceId);
			MonitorLog log = ((IPowerMonitorCapability) resource.getCapabilityByInterface(IPowerMonitorCapability.class))
					.getCurrentObservedLog();
			printMetrics(log);
		}catch (Exception e) {
			printError(" Error in getCurrentObservedPowerMetrics in resource " + 
					resourceId);
			printError(e);
		}

		printEndCommand();
		return null;

	}

	private void printMetrics(MonitorLog log) throws Exception{
			for (Load load: log.getEdlIncludeLoad()){
				// only show observed metrics
				if (!load.getEdlHasMeasurement().isEmpty()) {
					Measurement mm =load.getEdlHasMeasurement().iterator().next();
					String readTime = dateFormat.format(new Date(mm.getEdlTimestamp()*1000));
					printSymbol("Power Consumption Metrics for resource " + resourceId + " @ " + readTime + ": ");
					printSymbol(mm.getEdlMetricvalue() + load.getEdlMaptoMetric().getEdlHasUnit().getEdlUnitname());
				}
			}
	}		
}


