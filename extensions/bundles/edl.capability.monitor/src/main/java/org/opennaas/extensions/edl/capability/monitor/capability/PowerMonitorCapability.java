package org.opennaas.extensions.edl.capability.monitor.capability;

/*
 * #%L
 * OpenNaaS :: EDL ::PowerMonitor
 * %%
 * Copyright (C) 2007 - 2014 Fundaci�� Privada i2CAT, Internet i Innovaci�� a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.util.Date;

import javax.ws.rs.core.Response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.opennaas.core.resources.capability.CapabilityException;
import org.opennaas.core.resources.descriptor.CapabilityDescriptor;
import org.opennaas.extensions.edl.capability.monitor.controller.MeasurementCollector;
import org.opennaas.extensions.edl.capability.monitor.controller.PowerMonitorController;
import org.opennaas.extensions.edl.node.model.edl.EnergySource;
import org.opennaas.extensions.edl.node.model.edl.MonitorLog;
import org.opennaas.extensions.edl.node.Activator;
import org.codehaus.jackson.map.ObjectMapper;



public class PowerMonitorCapability extends AbstractPowerMonitorCapability 
implements IPowerMonitorCapability {

	private static Log						log				= LogFactory.getLog(PowerMonitorCapability.class);

	public static String CAPABILITY_TYPE = "edl_node_powermonitor";
	private String resourceId = "";
	private  MeasurementCollector  collector;
	
	public PowerMonitorCapability(CapabilityDescriptor descriptor, String resourceId) {
		super(descriptor);
		this.resourceId = resourceId;
		log.debug("Built new PDUPowerManagementCapability Capability");
	}
	
	@Override
	public void activate() throws CapabilityException {
		// try{
		// driver = instantiateDriver();
		// } catch (Exception e) {
		// throw new CapabilityException(e);
		// }
		registerService(Activator.getContext(), CAPABILITY_TYPE, getResourceType(), getResourceName(),
				IPowerMonitorCapability.class.getName());
		super.activate();
	}

	@Override
	public void deactivate() throws CapabilityException {
		collector = null;
		unregisterService();
		super.deactivate();
	}
	
	
	@Override
	public String getCapabilityName() {
		return CAPABILITY_TYPE;
	}

	@Override
	public MonitorLog getCurrentObservedLog() throws Exception {
		log.info("Get current monitor log of the resource locally");
		MonitorLog mlog = instantiateCollector().getCurrentObMonitorLog();
		if (mlog == null){
			throw new Exception("Empty monitor log");
		}
		return mlog;
	}

	@Override
	public Response getCurrentObservedLogRESTful() throws Exception {
		log.info("Get current monitor log of the resource through RESTful api");
		MonitorLog mlog = instantiateCollector().getCurrentObMonitorLog();
		if (mlog == null){
			return Response.serverError().entity("Current monitor log is empty").build();
		}
		
		String response = "No content";
        ObjectMapper mapper = new ObjectMapper();
        try {
            response = mapper.writeValueAsString(mlog);
        } catch (IOException ex) {
            Logger.getLogger(PowerMonitorCapability.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(response).build();
        
	}

	@Override
	public MonitorLog getLogbyTime(int period, int interval)
			throws Exception {
		log.info("Get monitor log of the resource for a period of time locally");
		MonitorLog mlog = instantiateCollector().getMonitorLogByTime(period, interval);
		if (mlog == null){
			throw new Exception("Empty monitor log");
		}
      
        return mlog;
        

	}
	
	@Override
	public Response getLogbyTimeRESTful(int period, int interval)
			throws Exception {
		log.info("Get monitor log of the resource for a period of time through RESTful api");
		MonitorLog mlog = instantiateCollector().getMonitorLogByTime(period, interval);
		if (mlog == null){
			return Response.serverError().entity("Current monitor log is empty").build();
		}
		
		String response = "No content";
        ObjectMapper mapper = new ObjectMapper();
        try {
            response = mapper.writeValueAsString(mlog);
        } catch (IOException ex) {
            Logger.getLogger(PowerMonitorCapability.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(response).build();
	}
	
	private MeasurementCollector instantiateCollector() throws Exception {
		if (collector == null)
			collector = PowerMonitorController.create(resourceId, 
					edlnodeId, descriptor);
		return collector;
		
	}
	
	@Override
	public void resyncModel() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getPriceRESTful() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getEmissionRESTful() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response getEnergySourceRESTful() throws Exception {
		log.info("Get current energy source through RESTful api");
		EnergySource es = instantiateCollector().getCurrentEnergySource();
		if (es == null){
			return Response.serverError().entity("Current energy source info is not set").build();
		}
		
		String response = "No content";
        ObjectMapper mapper = new ObjectMapper();
        try {
            response = mapper.writeValueAsString(es);
        } catch (IOException ex) {
            Logger.getLogger(PowerMonitorCapability.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(response).build();
	}

	@Override
	public EnergySource getEnergySource() throws Exception {
		log.info("Get current energy source locally");
		EnergySource es = instantiateCollector().getCurrentEnergySource();
		if (es == null){
			throw new Exception("Empty monitor log");
		}
		return es;
	}	
		
	

}
