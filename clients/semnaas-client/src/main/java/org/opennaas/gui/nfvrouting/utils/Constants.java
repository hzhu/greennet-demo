package org.opennaas.gui.nfvrouting.utils;

/**
 * @author Josep Batall�� (josep.batalle@i2cat.net)
 */
public class Constants {

	/* Rest constants */
	public static final String	WS_REST_URL             = "http://localhost:8888/opennaas/";
        public static final String	SDN_RESOURCE             = "sdn1";
        public static final String SAMPLE_RESOURCE 			= "resource1";
}
