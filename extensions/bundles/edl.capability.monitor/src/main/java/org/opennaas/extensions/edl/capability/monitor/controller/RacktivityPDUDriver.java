package org.opennaas.extensions.edl.capability.monitor.controller;

/*
 * #%L
 * OpenNaaS :: EDL ::PowerMonitor
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;


public class RacktivityPDUDriver implements GeneralPDUDriver{
	SNMPDriver snmpDriver;
	public static final String READ_COMMUNITY = "public"; 
	public static final String WRITE_COMMUNITY= "private"; 
	private String masterMIBbase = ".1.3.6.1.4.1.34097.9.77.1.1.";
	private String powerMIBbase= ".1.3.6.1.4.1.34097.9.80.1.1.";
//	private powermodule = {
//
//			"pCurrentTime"		  :   		[3, 'TimeTicks', 'string', 's', 0],
//			'pVoltage'    		  :         [4, 'Uhundredth', 'float',	'V', 0.01],
//			'pFrequency'  		  :         [5, 'Uthousandth', 'float', 'Hz', 0.01],
//			'pCurrent'    		  :         [6, 'Uthousandth', 'float', 'A', 0.001],
//			'pPower'      		  :         [7, 'Unsigned32', 'int', 'W', 1],
//			'pStatePortCur'		  :         [8, 'CURPORTSTATE', 'string', '', 0],
//			'pActiveEnergy' 	  :         [9, 'Uthousandth', 'float', 'kWh', 0.001],
//			'pApparentEnergy'	  :         [10, 'Unsigned32', 'int', 'W', 1],
//			'pTemperature'    	  :         [11, 'Utenth', 'float', 'K', 0.1],
//			'pApparentPower'   	  :         [15, 'Unsigned32', 'int', 'W', 1],
//			'pPowerFactor'        :         [16, 'Unsigned32', 'float', '', 0.01],
//			'pTotalCurrent'       :         [17, 'Uthousandth', 'float', 'A', 0.001],
//			'pTotalRealPower'     :         [18, 'Unsigned32', 'int', 'W', 1],
//			'pTotalApparentPower' :         [19, 'Unsigned32', 'int', 'W', 1],
//			'pTotalActiveEnergy'  :         [20, 'Uthousandth', 'float', 'kWh', 0.001],
//			'pTotalApparentEnergy':         [21, 'Uthousandth', 'float', 'kWh', 0.001],
//			'pTotalPowerFactor'   :         [22, 'Unsigned32', 'float', '', 0.01 ],
//			'pPortName'           :         [10034, 'DisplayString', 'string', 0],
//			'pPortState'          :         [10035, 'Unsigned32', 'int', 1],
//			'pModuleName'         :         [10001, 'DisplayString', 'string', 0],
//			'pFirmwareVersion'    :         [10002, 'Version', 'string', 0],
//			'pHardwareVersion'    :         [10003, 'Version', 'string', 0],
//			'pFirmwareID'         :         [10004, 'DisplayString', 'string', 0],
//			'pHardwareID'         :         [10005, 'DisplayString', 'string', 0]
//		}
//
//		private mastermodule = {
//			'mCurrentTime'        :   		[3, 'TimeTicks', 'string', 's',0],
//			'mTemperature'        :         [11, 'Utenth', 'float', 'K',0.1],
//			'mCurrentIP'          :         [14, 'IpAddress', 'string', '',0],
//			'mTotalCurrent'       :         [17, 'Uthousandth', 'float', 'A', 0.001],
//			'mTotalRealPower'     :         [18, 'Unsigned32', 'int', 'W', 1],
//			'mTotalActiveEnergy'  :         [20, 'Uthousandth', 'float', 'kWh', 0.001],
//			'mModuleName'         :         [10001, 'DisplayString', 'string', '', 0],
//			'mFirmwareVersion'    :         [10002, 'Version', 'string', '', 0],
//			'mHardwareVersion'    :         [10003, 'Version', 'string', '', 0],
//			'mFirmwareID'         :         [10004, 'DisplayString', 'string', '',0],
//			'mHardwareID'         :         [10005, 'DisplayString', 'string', '',0]
//		}
	
	public RacktivityPDUDriver(String pduDriverStr) throws IOException{
		String urladdress = pduDriverStr;
		snmpDriver = new SNMPDriver(urladdress);
		try {
			snmpDriver.setupListener();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public long getCurrentTime(int moduleIndex, int outletIndex) throws IOException{
		String oidEnd = "3."+ Integer.toString(moduleIndex); 
		String currentTimeOID = powerMIBbase + oidEnd;
		return snmpDriver.getSNMPToInteger(READ_COMMUNITY, currentTimeOID);
	}
	
	
	public String getOutletName(int moduleIndex, int outletIndex) throws IOException{
		String oidEnd = "10034." + Integer.toString(moduleIndex) +"."+ Integer.toString(outletIndex);
		String pPortNameOID = powerMIBbase + oidEnd;
		return snmpDriver.getSNMPToString(READ_COMMUNITY, pPortNameOID);
	}
	
	public String getMasterModuleName(int moduleIndex) throws IOException{
		String oidEnd = "10001." + Integer.toString(moduleIndex);
		String mModuleNameOID = masterMIBbase + oidEnd;
		return snmpDriver.getSNMPToString(READ_COMMUNITY, mModuleNameOID);
	}
	
	public String getModuleName(int moduleIndex, int outletIndex) throws IOException{
		String oidEnd = "10001." + Integer.toString(moduleIndex);
		String mModuleNameOID = powerMIBbase + oidEnd;
		return snmpDriver.getSNMPToString(READ_COMMUNITY, mModuleNameOID);
	}
	
	public String getPower(int moduleIndex, int outletIndex) throws IOException{
		String oidEnd = "7." + Integer.toString(moduleIndex) +"." + Integer.toString(outletIndex);
		String pPowerOID = powerMIBbase + oidEnd;
		return snmpDriver.getSNMPToString(READ_COMMUNITY, pPowerOID);
	}
	
	public String getEnergy(int moduleIndex, int outletIndex) throws IOException{
		String oidEnd = "9." + Integer.toString(moduleIndex) +"."+ Integer.toString(outletIndex);
		String pActiveEnergyOID = powerMIBbase + oidEnd;
		return snmpDriver.getSNMPToString(READ_COMMUNITY, pActiveEnergyOID);
	}
	
	public String getPowerFactor(int moduleIndex, int outletIndex) throws IOException{
		String oidEnd = "16." + Integer.toString(moduleIndex) +"."+ Integer.toString(outletIndex);
		String pPowerFactorOID = powerMIBbase + oidEnd;
		return snmpDriver.getSNMPToString(READ_COMMUNITY, pPowerFactorOID);
	}
	
	public Set<String> getMeasurement(int moduleIndex, int outletIndex) throws IOException{
		String oidEnd1 = "7." + Integer.toString(moduleIndex) +"."+ Integer.toString(outletIndex);
		String pPowerOID = powerMIBbase + oidEnd1;
		String oidEnd2 = "9." + Integer.toString(moduleIndex) +"."+ Integer.toString(outletIndex);
		String pActiveEnergyOID = powerMIBbase + oidEnd2;
		Set<String> oids = new LinkedHashSet<String>();
		oids.add(pPowerOID);
		oids.add(pActiveEnergyOID);
		return snmpDriver.getSNMPToStringSet(READ_COMMUNITY,oids);
	}
	
	public void powerOnOutlet(int moduleIndex, int outletIndex) throws IOException{
		String powerONOFFOID = powerMIBbase + "10035." + 
				Integer.toString(moduleIndex) +"."+ Integer.toString(outletIndex);
		snmpDriver.setSNMP(WRITE_COMMUNITY, powerONOFFOID, 1);
	}
	
	public void powerOffOutlet(int moduleIndex, int outletIndex) throws IOException{
		String powerONOFFOID = powerMIBbase + "10035." + 
				Integer.toString(moduleIndex) +"."+ Integer.toString(outletIndex);
		snmpDriver.setSNMP(WRITE_COMMUNITY, powerONOFFOID, 2);
	}

	@Override
	public String getNumOfOutlet(int moduleIndex, int outletIndex)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
	

}
