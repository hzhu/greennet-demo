package org.opennaas.extensions.edl.capability.monitor.capability;

/*
 * #%L
 * OpenNaaS :: EDL ::PowerMonitor
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.opennaas.extensions.edl.node.model.edl.EnergySource;
import org.opennaas.extensions.edl.node.model.edl.MonitorLog;
import org.opennaas.core.resources.capability.ICapability;

@Path("/")
public interface IPowerMonitorCapability extends ICapability{
	
	
	// get a electricity price of one energy source through RESTful API
	@Path("/price")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public String getPriceRESTful() throws Exception;
	
	// get a CO2 emission of one energy source through RESTful API
	@Path("/emission")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public String getEmissionRESTful() throws Exception;
	
	// get a electricity price of one energy source through RESTful API
	@Path("/energysource")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Response getEnergySourceRESTful() throws Exception;
	

	public EnergySource getEnergySource() throws Exception;
	
	// get a current monitor log in green metrics 
	// by observation from a power meter
	public MonitorLog getCurrentObservedLog() throws Exception;
	
	// get a current monitor log in green metrics 
	// by observation from a power meter through RESTful API
	@Path("/observedgmetric")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Response getCurrentObservedLogRESTful() throws Exception;
	
	
	// get a monitor log in specified green metrics
	// (including calculated and observed)  
	// from a power meter in a specified period
	public MonitorLog getLogbyTime(int period, int intv) 
			throws Exception;
	
	// get a monitor log in specified green metrics
	// (including calculated and observed)  
	// from a power meter in a specified period through RESTful API
	@Path("/gmetric/{period}/{intv}")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Response getLogbyTimeRESTful(@PathParam("period") int period, @PathParam("intv") int intv) 
			throws Exception;
}
