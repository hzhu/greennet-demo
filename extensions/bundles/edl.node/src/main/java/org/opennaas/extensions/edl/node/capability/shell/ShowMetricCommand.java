package org.opennaas.extensions.edl.node.capability.shell;

/*
 * #%L
 * OpenNaaS :: EDL :: NODE
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Set;

import org.apache.felix.gogo.commands.Argument;
import org.apache.felix.gogo.commands.Command;
import org.opennaas.core.resources.IResource;
import org.opennaas.core.resources.shell.GenericKarafCommand;
import org.opennaas.extensions.edl.node.model.edl.EDLNode;
import org.opennaas.extensions.edl.node.model.edl.EnergySource;
import org.opennaas.extensions.edl.node.model.edl.Load;

@Command(scope = "edl", name = "showMetric", 
	description = "Show the supported metric info")
public class ShowMetricCommand extends GenericKarafCommand{
	
	@Argument(index = 0, name = "resourceType:resourceName", 
			description = "The resource id", required = true, multiValued = false)
	private String	resourceName;


	@Override
	protected Object doExecute() throws Exception {
		
		printInitCommand("showMetric");
		try {
			IResource resource = getResourceFromFriendlyName(resourceName);
			printInfo("Suppored Metrics: ");
			String str = "{Observed [EnergyConsumption, PowerConsumption, PowerFactor] \n"
					+ "Calculated [EmissionEfficiency, EnergyEfficiency, TotalElectricityCost, TotalEmission]}";
			printSymbol(str);
			EDLNode node = (EDLNode) resource.getModel();
			printInfo("Already selected metrics: ");
			if (node.getEdlHasLog().getEdlIncludeLoad() != null){
				for(Load load: node.getEdlHasLog().getEdlIncludeLoad()){
					printSymbol(load.getEdlMaptoMetric().getEdlHasMetricName());
				}
			}
		} catch (Exception e) {
			printError("Error in show metric info " );
			printError(e);
		} finally {
			printEndCommand();
		}
		printEndCommand();
		return null;
	}
}
