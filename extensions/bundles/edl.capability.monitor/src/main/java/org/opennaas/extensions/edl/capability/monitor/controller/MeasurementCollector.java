package org.opennaas.extensions.edl.capability.monitor.controller;

/*
 * #%L
 * OpenNaaS :: EDL ::PowerMonitor
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import org.opennaas.core.resources.descriptor.CapabilityDescriptor;
import org.opennaas.extensions.edl.node.model.edl.Driver;
import org.opennaas.extensions.edl.node.model.edl.EDLNode;
import org.opennaas.extensions.edl.node.model.edl.EnergyConsumption;
import org.opennaas.extensions.edl.node.model.edl.EnergySource;
import org.opennaas.extensions.edl.node.model.edl.Load;
import org.opennaas.extensions.edl.node.model.edl.Measurement;
import org.opennaas.extensions.edl.node.model.edl.MonitorLog;
import org.opennaas.extensions.edl.node.model.edl.Outlet;
import org.opennaas.extensions.edl.node.model.edl.PowerConsumption;
import org.opennaas.extensions.edl.node.model.edl.PowerFactor;
import org.opennaas.extensions.edl.node.model.edl.PowerMeter;
import org.opennaas.extensions.edl.node.model.edl.Unit;
import org.opennaas.extensions.edl.node.model.edl.Metric;


public class MeasurementCollector {
	
	String pduName;
	private EDLNode node;
	private PowerMeter meter;
	private Outlet outlet;
	private GeneralPDUDriver pduDriver;
	private MonitorLog log;
	private Set<EnergySource> esSet;
	
	public Set<? extends GeneralPDUDriver> pduDrivers;

	public EDLNode getNode(){
		return node;
	}
	public void setNode(EDLNode nodePara){
		this.node = nodePara;
	}
	
	public MonitorLog getLog() throws Exception{
		log = node.getEdlHasLog();
		if (log == null){
			throw new Exception("Monitor log or its metrics are not defined!!");
		}
		return log;
	}
	
	public Set<EnergySource> getEnergySource() throws Exception{
		esSet = node.getEdlUseEnergySource();
		if (esSet == null){
			throw new Exception("Energy sources are not defined!!");
		}
		return esSet;
	}
	
	public PowerMeter getPowerMeter() throws Exception{
		meter = node.getEdlMonitoredBy();
		if (meter == null){
			throw new Exception("PowerMeter is not defined!!");
		}
		return meter;
	}

	public Outlet getOutlet() throws Exception{
		outlet = node.getEdlAttachTo();
		if (outlet == null){
			throw new Exception("Outlet is not defined!!");
		}
		return outlet;
	}
	
	public GeneralPDUDriver getPDUDriver() throws Exception{
		meter = getPowerMeter();
		if (meter.getEdlHasDriver() == null){
			throw new Exception("PowerMeter or its driver is not defined!!");
		}
		pduDriver = new RacktivityPDUDriver(meter.getEdlHasDriver().getEdlURLaddress());
		return pduDriver;
	}
//	public void setPDUDriver(Driver edlpduDriver) throws Exception{
//		this.pduDriver = new RacktivityPDUDriver();
//	}
	
	public EnergySource getCurrentEnergySource() throws Exception{
		esSet = getEnergySource();
		EnergySource currentES; 

		if (!esSet.isEmpty()){
			currentES= esSet.iterator().next();
		}else{
			throw new Exception("No energy source can be used!");
		}
		
		return currentES;
	}
	
	// store current measurements into the log
	public MonitorLog getCurrentObMonitorLog () throws Exception{
		Set<Load> loadset = new LinkedHashSet<Load>();
		log = getLog();
		outlet = getOutlet();
		try{
			for (Load load: log.getEdlIncludeLoad()){
				// initialize the measurement of the load
				Set<Measurement> mmset = new LinkedHashSet<Measurement>();
				if (load.getEdlMaptoMetric().getEdlHasMetricName().equals("PowerConsumption") ||
						load.getEdlMaptoMetric() instanceof PowerConsumption){
					mmset.add(getMeasurementByOutlet("power", outlet.getEdlModulenumber(), 
							outlet.getEdlPortnumber()));				
				}else if(load.getEdlMaptoMetric().getEdlHasMetricName().equals("EnergyConsumption") ||
						load.getEdlMaptoMetric() instanceof EnergyConsumption){
					mmset.add(getMeasurementByOutlet("energy", outlet.getEdlModulenumber(), 
							outlet.getEdlPortnumber()));
				}else if(load.getEdlMaptoMetric().getEdlHasMetricName().equals("PowerFactor") ||
						load.getEdlMaptoMetric() instanceof PowerFactor){
					mmset.add(getMeasurementByOutlet("powerfactor", outlet.getEdlModulenumber(), 
							outlet.getEdlPortnumber()));
				}
				load.setEdlHasMeasurement(mmset);
				loadset.add(load);
			}
		}catch (IOException ioe) {
			throw new Exception("Failed to read PowerMetrics. Error from by an outlet:", ioe);
		}	
		log.setEdlIncludeLoad(loadset);	
		return log;
	}
	
	// store measurements every an interval time in specified period into the log
	public MonitorLog getMonitorLogByTime(int period,
			int interval) throws Exception{
		Set<Load> loadset = new LinkedHashSet<Load>();
		log = getLog();
		outlet = getOutlet();
		Set <Measurement> powerMMSet = new LinkedHashSet<Measurement>();
		Set <Measurement> energyMMSet = new LinkedHashSet<Measurement>();
		Set <Measurement> pfMMSet = new LinkedHashSet<Measurement>();
		
		// obtain one measurement, then sleep interval*1000 time
		for (long time = 0; time <= period; time = time + interval){
			powerMMSet.add(getMeasurementByOutlet("power", outlet.getEdlModulenumber(), 
					outlet.getEdlPortnumber()));
			energyMMSet.add(getMeasurementByOutlet("energy", outlet.getEdlModulenumber(), 
						outlet.getEdlPortnumber()));
			pfMMSet.add(getMeasurementByOutlet("powerfactor", outlet.getEdlModulenumber(), 
					outlet.getEdlPortnumber()));			
			Thread.sleep(interval*1000);							
		}
		
		for (Load load: log.getEdlIncludeLoad()){
			if (load.getEdlMaptoMetric().getEdlHasMetricName().equals("PowerConsumption") ||
					load.getEdlMaptoMetric() instanceof PowerConsumption){
				load.setEdlHasMeasurement(powerMMSet);							
			}else if(load.getEdlMaptoMetric().getEdlHasMetricName().equals("EnergyConsumption") ||
					load.getEdlMaptoMetric() instanceof EnergyConsumption){
				load.setEdlHasMeasurement(energyMMSet);	
			}else if(load.getEdlMaptoMetric().getEdlHasMetricName().equals("PowerFactor") ||
					load.getEdlMaptoMetric() instanceof PowerFactor){
				load.setEdlHasMeasurement(pfMMSet);	
			}
			loadset.add(load);
		}
		log.setEdlIncludeLoad(loadset);
		log.setEdlSampleDuration(period);
		log.setEdlSampleInterval(interval);
		return log;
	}
	
	

	// read a observed measurement from the outlet
	public Measurement getMeasurementByOutlet(String label, int moduleId, int portId) throws Exception{
		String value = null;
		long timeStamp;
		pduDriver = getPDUDriver();
		Measurement mm = new Measurement();
		try{
			if (label.equals("power"))
				value = pduDriver.getPower(moduleId, portId);
			else if(label.equals("energy"))	
				value = pduDriver.getEnergy(moduleId, portId);
			else if(label.equals("powerfactor"))
				value = pduDriver.getPowerFactor(moduleId, portId);
			timeStamp = pduDriver.getCurrentTime(moduleId, portId);
		}catch (IOException ioe) {
			throw new Exception("Failed to read currentPowerMetrics. Error from Ractivity Driver:", ioe);
		}	
		mm.setEdlMetricvalue(Float.parseFloat(value));
		mm.setEdlTimestamp(timeStamp);
		System.out.println(label + ":" + value);
		return mm;
	}
	
//	
//	// store measurements every an interval time in specified period into the log
//	public MonitorLog getMonitorLogByTime(Datetime startDate, Datetime endDate,
//			int interval) throws Exception{
//		Set<Load> loadset = new LinkedHashSet<Load>();
//		log = getLog();
//		outlet = getOutlet();
//		Set <Measurement> mmset = new LinkedHashSet<Measurement>();
//		Set <Load> loadset = new LinkedHashSet<Load>();
//		Set <MonitorLog> mlogset = new LinkedHashSet<MonitorLog>();
//		long start = startDate.getTime();
//		long end = endDate.getTime();
//		
//		for (long time = start; time <= end; time = time + interval){
//			Measurement mm = getMeasurementByOutlet(outlet.getEdlModulenumber(),
//					outlet.getEdlPortnumber());
//			if ((mm.getEdlTimestamp() >= start)&&(mm.getEdlTimestamp() <= end))
//				mmset.add(mm);
//		}
//		
//		load.setEdlHasMeasurement(mmset);
//		loadset.add(load);
//		mlog.setEdlIncludeLoad(loadset);
//		mlog.setEdlSampleDuration((int)(long) (start-end));
//		mlog.setEdlSampleInterval(interval);
//		mlogset.add(mlog);
//		node.setEdlHasLog(mlogset);
//		return mlog;
//	}
	
	// read a set of observed measurement from the outlet by time
	public Set<Measurement> getMeasurementByOutletByTime(String label, int moduleNum, int portNum,
			Date startDate, Date endDate, int interval) throws Exception{
		String value;
		long timeStamp;
		Measurement mm = new Measurement();
		Set <Measurement> mmset = new LinkedHashSet<Measurement>();
		long start = startDate.getTime();
		long end = endDate.getTime();
		pduDriver = getPDUDriver();
		timeStamp = pduDriver.getCurrentTime(moduleNum, portNum);
		if (label == "power"){
			for (long time = start; time <= end; time = time + interval){
				value = pduDriver.getPower(moduleNum, portNum);
				mmset.add(setSingleMeasurement(value, timeStamp, mm));
			}
		}			
		else if(label == "powerfactor"){
			for (long time = start; time <= end; time = time + interval){
				value = pduDriver.getPowerFactor(moduleNum, portNum);
				mmset.add(setSingleMeasurement(value, timeStamp, mm));
			}
		}	
		else if(label == "energy"){
			for (long time = start; time <= end; time = time + interval){
				value = pduDriver.getEnergy(moduleNum, portNum);
				mmset.add(setSingleMeasurement(value, timeStamp, mm));
			}
		}
		else 
			throw new Exception("No proper API for PDU access found!");
		
		return mmset;
	}
	
	public Measurement setSingleMeasurement(String value, long timeStamp, Measurement mm){
		mm.setEdlMetricvalue(Float.parseFloat(value));
		mm.setEdlTimestamp(timeStamp);
		return mm;
	}
	
	//dump measurements into database

	public String dumpMeasurement(String nodeName){
		return null;
	}



}
