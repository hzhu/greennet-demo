package org.opennaas.extensions.edl.node.model.edl;

/*
 * #%L
 * OpenNaaS :: EDL :: NODE
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



//rdf("http://www.science.uva.nl/research/sne/edl.owl#EnergySource")
public class EnergySource{
	private Float elePrice;
	private Float emissionRate;
	private String energysourceName;
	
	//rdf("http://www.science.uva.nl/research/sne/edl.owl#electricityPrice")
	public Float getEdlElectricityPrice(){
		return elePrice;
	}
	public void setEdlElectricityPrice(Float edlElectricityPrice){
		this.elePrice = edlElectricityPrice;
	}

	//rdf("http://www.science.uva.nl/research/sne/edl.owl#emissionPerUnitofEnergy")
	public Float getEdlEmissionPerUnitofEnergy(){
		return emissionRate;
	}
	public void setEdlEmissionPerUnitofEnergy(Float edlEmissionPerUnitofEnergy){
		this.emissionRate = edlEmissionPerUnitofEnergy;
	}
	
	//rdf("http://www.science.uva.nl/research/sne/edl.owl#energyName")
	public String getEdlEnergyName(){
		return energysourceName;
	}
	public void setEdlEnergyName(String edlEnergyName){
		this.energysourceName = edlEnergyName;
	}

}
