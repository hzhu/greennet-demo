function ConvertJsonToPowerInfo(parsedJson) {
    
     //waiting(true);
                    
    var power; 
    var load;
    var unit;
    var powerinfoHtml
    

    if (parsedJson) {
        var isStringArray = typeof (parsedJson[0]) === 'string';
        load = parsedJson.edlIncludeLoad;
        for (i = 0; i< 3; i++) {
        	if (load.edlMaptoMetric.edlHasMetricName === 'PowerConsumption') {
        			power = load.edlHasMeasurement.edlMetricvalue;
        			unit = load.edlMaptoMetric.edlHasUnit.edlUnitname;
        	}
        }
        powerinfoHtml = '<b>Power Consumption: </b>' + power + unit;
        return powerinfoHtml;
        
    } 
    return null;
}

function showHidePreloader(show){
     if(show)
         document.getElementById('preloader').style.display='block';
     else
         document.getElementById('preloader').style.display='none';
}

function waiting(status){
    $body = $("body");
    if(status)
        $body.addClass("loading");
    else
        $body.removeClass("loading");
}

function removeFlowAll(){
    var table = document.getElementById("jsonFlowTable");
    try{
        for(var i = table.rows.length - 1; i > 0; i--){
           table.deleteRow(i);
      }
    }catch(e){}
}
